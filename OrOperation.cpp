#include "OrOperation.h"

OrOperation::OrOperation(const FrameGraph& frameGraph) : AbstrOperation(frameGraph)
{
}


std::string OrOperation::operator()(const Vertex& one, const Vertex& two,
                                const std::string& predicateName,
                                const bool& negativity)
{
    using namespace boost;
    property_map<Graph, negativity_t>::type nameMap =
            m_frameGraph.getNegPrprtMap();

    std::pair<bool, bool> oneResult =
            m_frameGraph.hasPredicate(one, predicateName);
    std::pair<bool, bool> twoResult =
            m_frameGraph.hasPredicate(two, predicateName);

    if ( oneResult.first && !twoResult.first )
        return nameMap[two];
    else if ( !oneResult.first && twoResult.first )
        return nameMap[one];
    else if ( oneResult.first && twoResult.first ) {
        if ( negativity ) {
            if ( !oneResult.second && !twoResult.second )
                return "false";
            else
                return "true";
        } else {
            if ( oneResult.second && twoResult.second )
                return "false";
            else
                return "true";
        }
    } else
        return "";
}
