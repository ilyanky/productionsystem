#include "parsetree.h"
#include <iostream>
#include <QDebug>

ParseTree::ParseTree()
{
    m_root = m_tree.add_vertex();
    m_currentNode = m_root;
}


void ParseTree::addValue(const QString& value)
{
    using namespace boost;
    typedef property_map<Tree, vertex_name_t>::type NameMap;

    NameMap nameMap = get(vertex_name, m_tree);
    if ( edgeCount(m_currentNode) < 2 ) {
        TreeVertex temp = m_tree.add_vertex();
        m_tree.add_edge(m_currentNode, temp);
        nameMap[temp] = value.toStdString();
    }
    else {
        qDebug() << "Error. Node contains more than 2 edges";
        return;
    }
}


void ParseTree::addOperation(const QString& oper)
{
    using namespace boost;
    typedef property_map<Tree, vertex_name_t>::type NameMap;

    NameMap nameMap = get(vertex_name, m_tree);
    if ( m_currentNode != m_root ) {
        if ( edgeCount(m_currentNode) < 2 ) {
            TreeVertex temp = m_tree.add_vertex();
            m_tree.add_edge(m_currentNode, temp);
            m_currentNode = temp;
        }
         else {
            qDebug() << "Error. Node contains more than 2 edges";
            return;
        }
    }
    else if ( nameMap[m_currentNode] != "" ) {
        if ( edgeCount(m_currentNode) < 2 ) {
            TreeVertex temp = m_tree.add_vertex();
            m_tree.add_edge(m_currentNode, temp);
            m_currentNode = temp;
        }
         else {
            qDebug() << "Error. Node contains more than 2 edges";
            return;
        }
    }

    nameMap[m_currentNode] = oper.toStdString();
}


int ParseTree::edgeCount(const TreeVertex& v)
{
    return boost::out_degree(v, m_tree);
}


void ParseTree::returnToParent()
{
    if ( m_currentNode == m_root ) {
        qDebug() << "There is no parent of this node";
        return;
    }

    using namespace boost;
    typedef typename boost::graph_traits<Tree>::in_edge_iterator inEdgeIter;

    std::pair<inEdgeIter, inEdgeIter> p = in_edges(m_currentNode, m_tree);
    m_currentNode = source(*(p.first), m_tree);
}


void ParseTree::showAll()
{
    using namespace boost;

    typedef property_map<Tree, vertex_index_t>::type IndexMap;
    IndexMap index = get(vertex_index, m_tree);

    property_map<Tree, vertex_name_t>::type nameMap =
            get(vertex_name, m_tree);

    std::cout << "vertices(g) = ";
    typedef graph_traits<Tree>::vertex_iterator vertex_iter;
    std::pair<vertex_iter, vertex_iter> vp;
    for (vp = vertices(m_tree); vp.first != vp.second; ++vp.first) {
        TreeVertex v = *vp.first;
        std::cout << index[v] <<  " ---- " << nameMap[v] << " ";
    }
    std::cout << std::endl;

    std::cout << "edges(g) = ";
    graph_traits<Tree>::edge_iterator ei, ei_end;
    for (tie(ei, ei_end) = edges(m_tree); ei != ei_end; ++ei)
        std::cout << "(" << index[source(*ei, m_tree)]
                  << "," << index[target(*ei, m_tree)] << ") ";
    std::cout << std::endl;
}


boost::property_map<Tree, boost::vertex_name_t>::type ParseTree::getNameMap()
{
    return boost::get(boost::vertex_name, m_tree);
}


std::pair<TreeOutEdgeIter, TreeOutEdgeIter> ParseTree::getOutEdges(const TreeVertex& v) const
{
    return boost::out_edges(v, m_tree);
}


bool ParseTree::isIdenity(const TreeVertex& one, const TreeVertex& two)
{
    using namespace boost;

    property_map<Tree, vertex_name_t>::type nameMap =
            get(vertex_name, m_tree);

    std::string oneName = nameMap[one];
    std::string twoName = nameMap[two];

    qDebug() << "---------";
    qDebug() << oneName.c_str();
    qDebug() << twoName.c_str();

    if ( isExpression(one) )
        oneName = getOperation( oneName, getPriorityOper(QString(oneName.c_str())) );
    if ( isExpression(two) )
        twoName = getOperation( twoName, getPriorityOper(QString(twoName.c_str())) );

    if ( oneName != twoName )
        return false;

    if ( edgeCount(one) != edgeCount(two) )
        return false;

    std::pair<TreeVertex, TreeVertex> p_one = getNodes(one);
    std::pair<TreeVertex, TreeVertex> p_two = getNodes(two);

//    qDebug() << nameMap[p_one.first].c_str();
//    qDebug() << nameMap[p_one.second].c_str();

//    qDebug() << nameMap[p_two.first].c_str();
//    qDebug() << nameMap[p_two.second].c_str();

    if ( !isIdenity(p_one.first, p_two.first) ) {
        if ( !isIdenity(p_one.first, p_two.second) )
            return false;
        else if ( !isIdenity(p_one.second, p_two.first) )
            return false;
        else
            return true;
    }
    else if ( !isIdenity(p_one.second, p_two.second) )
        return false;
    else
        return true;
}


bool ParseTree::isExpression(const TreeVertex& v)
{
    using namespace boost;

    property_map<Tree, vertex_name_t>::type nameMap =
            get(vertex_name, m_tree);

    std::string name = nameMap[v];
    if ( name == "not" )
        return false;
    if ( name == "or" )
        return false;
    if ( name == "and" )
        return false;

    if ( containsOper(QString(name.c_str()), 0, name.size()) )
        return true;

    return false;
}


std::pair<TreeVertex, TreeVertex> ParseTree::getNodes(const TreeVertex& v)
{
    using namespace boost;

    std::pair<TreeVertex, TreeVertex> result;
    std::pair<TreeOutEdgeIter, TreeOutEdgeIter> p = out_edges(v, m_tree);
    result.first = target(*p.first, m_tree);
    ++p.first;
    if ( p.first != p.second )
        result.second = target(*p.first, m_tree);

    return result;
}


bool ParseTree::helper()
{

    using namespace boost;

    typedef property_map<Tree, vertex_index_t>::type IndexMap;
    IndexMap index = get(vertex_index, m_tree);

    property_map<Tree, vertex_name_t>::type nameMap =
            get(vertex_name, m_tree);

    typedef graph_traits<Tree>::vertex_iterator vertex_iter;
    std::pair<vertex_iter, vertex_iter> vp = vertices(m_tree);
//    for (vp = vertices(m_tree); vp.first != vp.second; ++vp.first) {
//        Vertex v = *vp.first;
//        std::cout << index[v] <<  " ---- " << nameMap[v] << " ";
//    }
//    std::cout << std::endl;

    TreeVertex v = *vp.first;
    ++vp.first;
//    ++vp.first;
    TreeVertex u = *(++vp.first);

    qDebug() << nameMap[v].c_str() << nameMap[u].c_str();

    return isIdenity(v, u);
}


TreeVertex ParseTree::getRoot() const
{
    return m_root;
}


TreeVertex ParseTree::getTarget(const TreeEdge& edge) const
{
    using namespace boost;

    return target(edge, m_tree);
}


std::string ParseTree::getVertexName(const TreeVertex& v)
{
    using namespace boost;
    property_map<Tree, vertex_name_t>::type nameMap =
            get(vertex_name, m_tree);


    return nameMap[v];
}


void ParseTree::clear()
{
    m_tree.clear();
    m_root = m_tree.add_vertex();
    m_currentNode = m_root;
}
