#ifndef PARSETREE_H
#define PARSETREE_H

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/directed_graph.hpp>
#include <QMap>
#include <QVector>
#include <QStringList>
#include <QString>
#include <QPair>
#include <HelpAlgorithms.h>

typedef typename boost::directed_graph<
    boost::property<boost::vertex_name_t, std::string>
    >  Tree;
typedef typename boost::graph_traits<Tree>::vertex_descriptor  TreeVertex;
typedef typename boost::graph_traits<Tree>::edge_descriptor    TreeEdge;
typedef typename boost::graph_traits<Tree>::out_edge_iterator  TreeOutEdgeIter;

class ParseTree
{

private:
    Tree m_tree;
    TreeVertex m_currentNode;
    TreeVertex m_root;
    
    int edgeCount(const TreeVertex& v);
    bool hasChildren(const TreeVertex& v);

public:
    ParseTree();
    void addValue(const QString& value);
    void addOperation(const QString& oper);
    void returnToParent();
    void showAll();
    boost::property_map<Tree, boost::vertex_name_t>::type getNameMap();
    std::pair<TreeOutEdgeIter, TreeOutEdgeIter> getOutEdges(const TreeVertex& v) const;
    bool isIdenity(const TreeVertex& one, const TreeVertex& two);
    bool isExpression(const TreeVertex& v);
    std::pair<TreeVertex, TreeVertex> getNodes(const TreeVertex& v);
    bool helper();
    TreeVertex getRoot() const;
    TreeVertex getTarget(const TreeEdge& edge) const;
    std::string getVertexName(const TreeVertex& v);
    void clear();
};

#endif // PARSETREE_H
