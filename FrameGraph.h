#ifndef FRAMEGRAPH_H
#define FRAMEGRAPH_H

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/property_maps/container_property_map.hpp>
#include <QMap>
#include <QVector>
#include <QStringList>
#include <QString>
#include <parsetree.h>
//#include <OrOperation.h>


enum LinkType {
    ISA,
    CONTAINS,
    IF
};


struct VertexInfo {
    std::string name;
    std::vector<std::string> attrs;
    LinkType linkType;

    bool operator==(const VertexInfo& other) const
    {
        return this->name == other.name;
    }


    bool operator<(const VertexInfo& other) const
    {
        return this->name < other.name;
    }
};




struct negativity_t {
  typedef boost::edge_property_tag kind;
};

typedef boost::property<negativity_t, bool> NegativityPrprt;
typedef boost::property<boost::edge_name_t, LinkType,
    NegativityPrprt> EdgePrprt;




struct attributes_t {
    typedef boost::vertex_property_tag kind;
};

typedef boost::property<attributes_t, std::vector<std::string> > AttrsPrprt;
typedef boost::property<boost::vertex_name_t, std::string,
    boost::property<boost::vertex_index_t, std::size_t,
    AttrsPrprt> > VertexPrprt;



typedef typename boost::adjacency_list<
    boost::setS,
    boost::setS,
    boost::directedS,
    VertexPrprt,
    EdgePrprt
    >  Graph;
typedef typename boost::graph_traits<Graph>::vertex_descriptor  Vertex;
typedef typename boost::graph_traits<Graph>::edge_descriptor    Edge;
typedef typename boost::graph_traits<Graph>::vertex_iterator    VertexIter;
typedef typename boost::graph_traits<Graph>::out_edge_iterator  OutEdgeIter;




class FeatherGraph
{

private:
//    typedef QVector<std::pair<int, int> >  EdgeVector;

    Graph                              m_graph;
    //QMap<std::string, Vertex>          m_vertices;
    //QMap<Vertex, QStringList>          m_frameBuffer;
    //QString                            m_KBname;
    
    std::set<std::string> m_tokens;

public:
            FeatherGraph();
//    void    putFrame(const QString& id, const QStringList& attrs);
    Vertex  addVertex(const std::string& vertexName);
    Edge    addEdge(const Vertex& from, const Vertex& to, const LinkType& type);

    void show();
    void show2();
    void showNames();
    std::pair<bool, Vertex>    contains(const std::string& name);

    void                       insertParseResult(const TreeVertex& node, ParseTree& tree, Vertex& v);
    std::pair<Vertex, Vertex>  getTwoAdjacent(const Vertex& v);
    void                       resolveOperations(const std::string& predicateName);
    std::pair<bool, bool>      hasPredicate(const Vertex& v, const std::string& name);
    std::pair<bool, Vertex>    unresolved(const std::list<Vertex>& list);
    std::string                xorOperation(const Vertex& one, const Vertex& two,
                                            const std::string& predicateName,
                                            const bool& negativity);
    Vertex                     addPredicate(const std::string& name,
                                            const std::vector<std::string>& attrs);
    Vertex                     getOneAdjcntVertex(const Vertex& v);
    void                       removeVertex(Vertex v);

    void                       convolve(const Vertex& vertex,
                                        std::set<VertexInfo>* const info, const LinkType& link);
    void                       addAttr(const std::string& vertexName, const std::string& attr);
    std::list<Vertex>          getVertices(const std::string& predicateName);

    std::pair<OutEdgeIter, OutEdgeIter> getOutEdges(const Vertex& vertex);
    std::string targetName(const Edge& edge);
    std::string sourceName(const Edge& edge);
    std::string getName(const Vertex& v);

    void if_link(const std::string& predicateName, const std::string& name);
    void isa_link(const std::string& predicateName, const std::string& name);
    void contains_link(const std::string& predicateName, const std::string& name);
};

#endif // FRAMEGRAPH_H
