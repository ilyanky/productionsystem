#ifndef OROPERATION_H
#define OROPERATION_H

#include <AbstrOperation.h>

class OrOperation : public AbstrOperation
{
public:
    OrOperation(const FrameGraph& frameGraph);
    std::string operator()(const Vertex& one, const Vertex& two,
                       const std::string& predicateName,
                       const bool& negativity);
};

#endif // OROPERATION_H
