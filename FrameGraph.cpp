#include "FrameGraph.h"
#include <QFile>
#include <iostream>
#include <QDataStream>
#include <QDebug>
#include <QVector>
#include <boost/property_map/property_map.hpp>


FeatherGraph::FeatherGraph()
{
    m_tokens.insert("not");
    m_tokens.insert("or");
    m_tokens.insert("and");
}


Vertex FeatherGraph::addVertex(const std::string& vertexName)
{
    using namespace boost;
    property_map<Graph, vertex_name_t>::type nameMap =
            get(vertex_name, m_graph);
    property_map<Graph, vertex_index_t>::type indexMap =
            get(vertex_index, m_graph);

    Vertex v;
    std::pair<VertexIter, VertexIter> pv = vertices(m_graph);
    for ( ; pv.first != pv.second; ++pv.first )
        if ( nameMap[*pv.first] == vertexName )
            return *pv.first;

    v = add_vertex(m_graph);
    nameMap[v] = vertexName;
    indexMap[v] = num_vertices(m_graph) - 1;
    //m_vertices.insert(vertexName, v);

    return v;
}


Edge FeatherGraph::addEdge(const Vertex& from, const Vertex& to, const LinkType& type)
/* neg = false by default */
{
    using namespace boost;
    property_map<Graph, edge_name_t>::type nameMap =
            get(edge_name, m_graph);

    bool inserted;
    Edge edge;
    tie(edge, inserted) = add_edge(from, to, m_graph);
    nameMap[edge] = type;

    return edge;
}


void FeatherGraph::insertParseResult(const TreeVertex& node, /*!!!const*/ ParseTree& tree, Vertex& v)
{
    using namespace boost;

    std::pair<bool, Vertex> result = contains(tree.getVertexName(node));
    if ( result.first ) {
        add_edge(v, result.second, m_graph);
        return;
    }

    Vertex temp = addVertex(tree.getVertexName(node));

    std::pair<TreeOutEdgeIter, TreeOutEdgeIter> p = tree.getOutEdges(node);
    while ( p.first != p.second ) {
        insertParseResult(tree.getTarget(*(p.first)), tree, temp);
        ++p.first;
    }

    add_edge(v, temp, m_graph);
}


void FeatherGraph::show()
{
    using namespace boost;
    property_map<Graph, vertex_index_t>::type indexMap =
            get(vertex_index, m_graph);

    std::cout << "edges(g) = ";
    graph_traits<Graph>::edge_iterator ei, ei_end;
    for (tie(ei, ei_end) = edges(m_graph); ei != ei_end; ++ei)
        std::cout << "(" << indexMap[source(*ei, m_graph)]
                  << "," << indexMap[target(*ei, m_graph)] << ") ";
    std::cout << std::endl;
}


void FeatherGraph::show2()
{
    using namespace boost;
    property_map<Graph, vertex_name_t>::type nameMap =
            get(vertex_name, m_graph);

    std::cout << "edges(g) = ";
    graph_traits<Graph>::edge_iterator ei, ei_end;
    for (tie(ei, ei_end) = edges(m_graph); ei != ei_end; ++ei)
        std::cout << "(" << nameMap[source(*ei, m_graph)]
                  << "," << nameMap[target(*ei, m_graph)] << ") ";
    std::cout << std::endl;
}


void FeatherGraph::showNames()
{
    using namespace boost;
    property_map<Graph, vertex_index_t>::type indexMap =
            get(vertex_index, m_graph);
    property_map<Graph, vertex_name_t>::type nameMap =
            get(vertex_name, m_graph);

    std::cout << "vertices(g) = ";
    typedef graph_traits<Graph>::vertex_iterator vertex_iter;
    std::pair<vertex_iter, vertex_iter> vp;
    for (vp = vertices(m_graph); vp.first != vp.second; ++vp.first) {
        Vertex v = *vp.first;
        std::cout << indexMap[v] <<  " ---- " << nameMap[v] << " ";
    }
    std::cout << std::endl;
}


std::pair<bool, Vertex> FeatherGraph::contains(const std::string& name)
{
    //qDebug() << "Contains called";
    using namespace boost;

    if ( m_tokens.count(name) )
        return std::pair<bool, Vertex>(false, Vertex());

    property_map<Graph, vertex_name_t>::type nameMap =
            get(vertex_name, m_graph);

    std::pair<VertexIter, VertexIter> vp;
    for ( vp = vertices(m_graph); vp.first != vp.second; ++vp.first ) {
        Vertex v = *vp.first;
        if ( nameMap[v] == name )
            return std::pair<bool, Vertex>(true, v);
    }

    return std::pair<bool, Vertex>(false, Vertex());
}


std::pair<Vertex, Vertex> FeatherGraph::getTwoAdjacent(const Vertex& v)
{
    qDebug() << "getTwoAdjacent called";
    using namespace boost;

    std::pair<Vertex, Vertex> result;
    std::pair<OutEdgeIter, OutEdgeIter> pi = out_edges(v, m_graph);
    result.first = target(*pi.first, m_graph);
    ++pi.first;
    if ( pi.first != pi.second ) //проверить
        result.second = target(*pi.first, m_graph);
    else
        pi.second = pi.first;

    return result;
}


void FeatherGraph::resolveOperations(const std::string& predicateName)
{
    qDebug() << "resolveOperations called";
    using namespace boost;

    std::list<Vertex> list;
    property_map<Graph, vertex_name_t>::type nameMap =
            get(vertex_name, m_graph);

    std::pair<VertexIter, VertexIter> vp;
    for ( vp = vertices(m_graph); vp.first != vp.second; ++vp.first ) {
        Vertex v = *vp.first;
        if ( m_tokens.count(nameMap[v]) ) //containsOper
            list.push_back(v);
    }


//    typedef graph_traits<Graph>::edge_iterator EdgeIter;
    std::pair<bool, Vertex> unres = unresolved(list);
    while ( unres.first ) {
        std::pair<Vertex, Vertex> operands = getTwoAdjacent(unres.second);
        std::string result = xorOperation(operands.first,
                                     operands.second,
                                     predicateName, false);
        if ( result != "" ) {
            //Vertex temp = source(unres.second, m_graph);
            nameMap[unres.second] = result;
            clear_vertex(operands.first, m_graph);
            clear_vertex(operands.second, m_graph);
            remove_vertex(operands.first, m_graph);
            remove_vertex(operands.second, m_graph);
        }
        list.remove(unres.second);

        unres = unresolved(list);
    }

}


std::pair<bool, bool> FeatherGraph::hasPredicate(const Vertex& v, const std::string& name)
{
    qDebug() << "hasPredicate called";
    using namespace boost;
    property_map<Graph, vertex_name_t>::type nameMap =
            get(vertex_name, m_graph);

    std::pair<bool, bool> result;
    result.first = false;
    result.second = false;

    std::pair<VertexIter, VertexIter> pv = vertices(m_graph);
    Vertex predicateVertex = v;
    for ( ; pv.first != pv.second; ++pv.first ) {
        if ( nameMap[*pv.first] == name )
            predicateVertex = *pv.first;
    }
    if ( predicateVertex == v )
        return result;

    std::pair<OutEdgeIter, OutEdgeIter> pe =
            out_edges(predicateVertex, m_graph);
    for ( ; pe.first != pe.second; ++pe.first ) {
        Vertex temp = target(*pe.first, m_graph);
        if ( nameMap[temp] == nameMap[v] ) {
            result.first = true;
            break;
        }
        //проверка на negative
    }

    return result;
}


std::pair<bool, Vertex> FeatherGraph::unresolved(const std::list<Vertex>& list)
{
    qDebug() << "unresolved called";
    boost::property_map<Graph, boost::vertex_name_t>::type nameMap =
            boost::get(boost::vertex_name, m_graph);
    boost::property_map<Graph, boost::vertex_index_t>::type indexMap =
            boost::get(boost::vertex_index, m_graph);

    for ( auto i : list ) {
        std::pair<Vertex, Vertex> operands = getTwoAdjacent(i);
        QString one = nameMap[operands.first].c_str();
        QString two = nameMap[operands.second].c_str();
        if ( !containsOper(one, 0, one.size()-1) && !containsOper(two, 0, two.size()-1) ) {
            qDebug() << "\tVertex index: " << indexMap[i];
            return std::pair<bool, Vertex>(true, i);
        }
    }

    qDebug() << "\tthere is no unresolved operations";
    return std::pair<bool, Vertex>(false, Vertex());
}


std::string FeatherGraph::xorOperation(const Vertex& one, const Vertex& two,
                                const std::string& predicateName,
                                const bool& negativity)
{
    qDebug() << "orOperation called";
    using namespace boost;
    property_map<Graph, vertex_name_t>::type nameMap =
            get(vertex_name, m_graph);

    std::pair<bool, bool> oneResult =
            hasPredicate(one, predicateName);
    std::pair<bool, bool> twoResult =
            hasPredicate(two, predicateName);

    if ( oneResult.first && !twoResult.first ) {
        qDebug() << "\tfirst if: " << nameMap[two].c_str();
        return nameMap[two];
    }
    else if ( !oneResult.first && twoResult.first ) {
        qDebug() << "\tsecond if: " << nameMap[one].c_str();
        return nameMap[one];
    }
    else if ( oneResult.first && twoResult.first ) {
        if ( negativity ) {
            if ( !oneResult.second && !twoResult.second ) {
                qDebug() << "\tthird if: " << "false";
                return "false";
            }
            else {
                qDebug() << "\tfourth if: " << "true";
                return "true";
            }
        } else {
            if ( oneResult.second && twoResult.second ) {
                qDebug() << "\tfifth if: " << "false";
                return "false";
            }
            else {
                qDebug() << "\tsixth if: " << "true";
                return "true";
            }
        }
    } else {
        qDebug() << "\tseventh if: " << "";
        return "";
    }
}


Vertex FeatherGraph::addPredicate(const std::string& name,
                                const std::vector<std::string>& attrs)
{
    using namespace boost;
    property_map<Graph, vertex_name_t>::type nameMap =
            get(vertex_name, m_graph);

    std::pair<VertexIter, VertexIter> pv = vertices(m_graph);
    for ( ; pv.first != pv.second; ++pv.first ) {
        if ( nameMap[*pv.first] == name )
            return *pv.first;
    }

    property_map<Graph, attributes_t>::type attrsMap =
            get(attributes_t(), m_graph);
    Vertex v = addVertex(name);
    attrsMap[v] = attrs;

    return v;
}


Vertex FeatherGraph::getOneAdjcntVertex(const Vertex& v)
{
    using namespace boost;

    std::pair<OutEdgeIter, OutEdgeIter> pe = out_edges(v, m_graph);

    return target(*pe.first, m_graph);
}


void FeatherGraph::removeVertex(Vertex v)
{
    using namespace boost;

    clear_vertex(v, m_graph);
    remove_vertex(v, m_graph);
    v = NULL;
}


void FeatherGraph::convolve(const Vertex& vertex,
                          std::set<VertexInfo>* const info, const LinkType& link)
{
    using namespace boost;
    property_map<Graph, edge_name_t>::type linkMap =
            get(edge_name, m_graph);
    std::pair<OutEdgeIter, OutEdgeIter> pe =
            out_edges(vertex, m_graph);


    for ( ; pe.first != pe.second; ++pe.first )
        convolve(target(*pe.first, m_graph), info, linkMap[*pe.first]);


    property_map<Graph, vertex_name_t>::type nameMap =
            get(vertex_name, m_graph);
    property_map<Graph, attributes_t>::type attrsMap =
            get(attributes_t(), m_graph);
    VertexInfo i;
    i.name = nameMap[vertex];
    i.attrs = attrsMap[vertex];
    i.linkType = link;
    info->insert(i);
}


void FeatherGraph::addAttr(const std::string& vertexName, const std::string& attr)
{
    using namespace boost;
    property_map<Graph, vertex_name_t>::type nameMap =
            get(vertex_name, m_graph);

    std::pair<VertexIter, VertexIter> pv = vertices(m_graph);
    for ( ; pv.first != pv.second; ++pv.first )
        if ( nameMap[*pv.first] == vertexName )
            break;
    if ( pv.first == pv.second )
        return;

    property_map<Graph, attributes_t>::type attrMap =
            get(attributes_t(), m_graph);
    attrMap[*pv.first].push_back(attr);
}


void FeatherGraph::if_link(const std::string& predicateName, const std::string& name)
{
    Vertex v = addVertex(name);
    Vertex d = addPredicate(predicateName, std::vector<std::string>());
    addEdge(v, d, IF);
}


void FeatherGraph::isa_link(const std::string& predicateName, const std::string& name)
{
    std::pair<bool, Vertex> first = contains(predicateName);
    std::pair<bool, Vertex> second = contains(name);

    if ( first.first && second.first ) {
        std::set<VertexInfo> info;
        convolve(first.second, &info, ISA);
        for ( auto i : info )
            if ( i.name == name ) {
                qDebug() << "cycle prevented";
                return;
            }
    }

    Vertex v = addVertex(name);

    std::vector<std::string> attr;
    attr.push_back(QString(predicateName.c_str()).toLower().toStdString());
    Vertex d = addPredicate(predicateName, attr);

    addEdge(v, d, ISA);
}


std::list<Vertex> FeatherGraph::getVertices(const std::string& predicateName)
{
    using namespace boost;
    typedef graph_traits<Graph>::edge_iterator EdgeIter;
    property_map<Graph, vertex_name_t>::type nameMap =
            get(vertex_name, m_graph);

    std::list<Vertex> list;
    std::pair<EdgeIter, EdgeIter> pe = edges(m_graph);
    for ( ; pe.first != pe.second; ++pe.first )
        if ( nameMap[target(*pe.first, m_graph)] == predicateName )
            list.push_back(source(*pe.first, m_graph));

    return list;
}


std::pair<OutEdgeIter, OutEdgeIter> FeatherGraph::getOutEdges(const Vertex& vertex)
{
    return boost::out_edges(vertex, m_graph);
}


std::string FeatherGraph::targetName(const Edge& edge)
{
    using namespace boost;
    property_map<Graph, vertex_name_t>::type nameMap =
            get(vertex_name, m_graph);

    return nameMap[target(edge, m_graph)];
}


std::string FeatherGraph::sourceName(const Edge& edge)
{
    using namespace boost;
    property_map<Graph, vertex_name_t>::type nameMap =
            get(vertex_name, m_graph);

    return nameMap[source(edge, m_graph)];
}


std::string FeatherGraph::getName(const Vertex& v)
{
    using namespace boost;
    property_map<Graph, vertex_name_t>::type nameMap =
            get(vertex_name, m_graph);

    return nameMap[v];
}


void FeatherGraph::contains_link(const std::string& predicateName, const std::string& name)
{
    std::pair<bool, Vertex> first = contains(predicateName);
    std::pair<bool, Vertex> second = contains(name);

    if ( first.first && second.first ) {
        std::set<VertexInfo> info;
        convolve(first.second, &info, CONTAINS);
        for ( auto i : info )
            if ( i.name == name ) {
                qDebug() << "cycle prevented";
                return;
            }
    }

    Vertex v = addVertex(name);

    std::vector<std::string> attr;
    attr.push_back(QString(predicateName.c_str()).toLower().toStdString());
    Vertex d = addPredicate(predicateName, attr);

    addEdge(v, d, CONTAINS);
}
