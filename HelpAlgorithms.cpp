#include "HelpAlgorithms.h"

int getPriorityOper(const QString& exp)
{
    QString copyExp = exp;
    copyExp.push_front('(');
    copyExp.push_back(')');

//    qDebug() << copyExp;
    QVector<int> brakets;
    QVector<int> operations;

    for ( int i = 0; i < copyExp.size(); ++i ) {
        if ( copyExp.at(i) == '(' )
            brakets.push_back(i);

        if ( copyExp.at(i) == ')' )
            brakets.push_back(i);
    }

//    qDebug() << brakets;
    int level = 1;
    while ( level <= brakets.size() / 2 ) {
        int currLevel = 1;
        for ( int i = 0; i < brakets.size() - 1; ++i) {
            if ( currLevel == level ) {
                int beginBr = brakets[i];
                int endBr = brakets[i + 1];
                if ( containsOper(copyExp, beginBr, endBr) ) {
                    int index = beginBr;
                    while ( index < endBr ) {
                        index = copyExp.indexOf("or", index);
                        if ( index == -1 || index > endBr )
                            break;
                        else {
                            operations.push_back(index);
                            ++index;
                        }
                    }

                    index = beginBr;
                    while ( index < endBr ) {
                        index = copyExp.indexOf("and", index);
                        if ( index == -1 || index > endBr )
                            break;
                        else {
                            operations.push_back(index);
                            ++index;
                        }
                    }

                    index = beginBr;
                    while ( index < endBr ) {
                        index = copyExp.indexOf("not", index);
                        if ( index == -1 || index > endBr )
                            break;
                        else {
                            operations.push_back(index);
                            ++index;
                        }
                    }
                }
            }

            if ( copyExp.at(brakets[i + 1]) == ')' )
                --currLevel;
            else if ( copyExp.at(brakets[i + 1]) == '(' )
                ++currLevel;


//            qDebug() << level << currLevel;
        }

        if (operations.size() != 0)
            break;

        ++level;
    }


    if ( operations.size() != 0 ) {
        qSort(operations.begin(), operations.end(), qGreater<int>());
//        qDebug() << operations;

        if ( containsOper(copyExp, 0, copyExp.size() - 1, Or) ) {
            for ( int i = 0; i < operations.size(); ++i ) {
                if ( copyExp.at(operations[i]) == 'o' )
                    return operations[i] - 1;
            }
        }
        if ( containsOper(copyExp, 0, copyExp.size() - 1, And) ) {
            for ( int i = 0; i < operations.size(); ++i ) {
                if ( copyExp.at(operations[i]) == 'a' )
                    return operations[i] - 1;
            }
        }
        if ( containsOper(copyExp, 0, copyExp.size() - 1, Not) ) {
            for ( int i = 0; i < operations.size(); ++i ) {
                if ( copyExp.at(operations[i]) == 'n' )
                    return operations[i] - 1;
            }
        }
    }
    else { //-----unnecessary most likely
        if ( containsOper(copyExp, 0, copyExp.size() - 1, Or) )
            return copyExp.lastIndexOf("or");
        else if ( containsOper(copyExp, 0, copyExp.size() - 1, And) )
            return copyExp.lastIndexOf("and");
        else /*if ( containsOper(exp, 0, exp.size() - 1, Not) )*/
            return copyExp.lastIndexOf("not");
    }

    return -1;
}


bool containsOper(const QString& exp, const int& beginIndex,
                  const int& endIndex, const Operation& oper)
/*oper = Nothing by default*/
{
    if ( oper == Nothing ) {
        int temp = exp.indexOf("not", beginIndex);
        if ( temp != -1 && temp < endIndex )
            return true;

        temp = exp.indexOf("or", beginIndex);
        if ( temp != -1 && temp < endIndex )
            return true;

        temp = exp.indexOf("and", beginIndex);
        if ( temp != -1 && temp < endIndex )
            return true;

        return false;
    }
    else if ( oper == Or ) {
        int temp = exp.indexOf("or", beginIndex);
        if ( temp != -1 && temp < endIndex )
            return true;
        else
            return false;
    }
    else if ( oper == And ) {
        int temp = exp.indexOf("and", beginIndex);
        if ( temp != -1 && temp < endIndex )
            return true;
        else
            return false;
    }
    else /*if ( oper == Not )*/ {
        int temp = exp.indexOf("not", beginIndex);
        if ( temp != -1 && temp < endIndex )
            return true;
        else
            return false;
    }
}


std::string getOperation(const std::string& exp, const int& index)
{       
    qDebug() << exp.c_str() << index;

    if ( exp.at(index) == 'n' )
        return std::string("not");
    if ( exp.at(index) == 'a' )
        return std::string("and");
    if ( exp.at(index) == 'o' )
        return std::string("or");

    return std::string();
}
