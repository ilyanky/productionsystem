#ifndef PILLOWSYSTEM_H
#define PILLOWSYSTEM_H

#include <QString>
#include <QSet>
#include <FrameGraph.h>
//#include <parsetree.h>

enum Side {
    Left,
    Right
};

class PillowSystem
{
private:

    FeatherGraph m_frameGraph;

    FeatherGraph m_rulesGraph;
    std::map<std::string, Vertex> m_rules;

    ParseTree m_parseTree;

public:
    PillowSystem();
    void tell(const QString& exp, const LinkType& type);
    void parseExpression(const QString& exp);
    bool isErrorExpression(const QString& exp);
    QString getSideValue(const QString& exp, const int& index, const Side& side);
    bool isAtomic(const QString& exp);
    std::string results(const std::string& vertexName);
    void addAttr(const std::string& vertexName, const std::string& attr);
    void tellRule(const std::string& vertexName, const std::string& ruleName);
    void analyseRules(const std::string& temp);
    
    void tempShow();
    bool tempIsIdenity();
    void showGraph();
    void showGraph2();
    void showGraphNames();

    void showRuleGraph2();
    void showRuleGraphNames();
    

};
#endif // PILLOWSYSTEM_H
