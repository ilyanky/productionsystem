#ifndef HELPALGORITHMS_H
#define HELPALGORITHMS_H

#include <QString>
#include <QVector>
#include <string>
#include <QDebug>

enum Operation {
    Or,
    And,
    Not,
    Nothing
};

int  getPriorityOper(const QString& exp);
bool containsOper(const QString& exp, const int& beginIndex, const int& endIndex,
                  const Operation& oper = Nothing);
std::string getOperation(const std::string& exp, const int& index);

#endif // HELPALGORITHMS_H
