#include <QDebug>
#include <PillowSystem.h>

void ThalesTheorem()
{
    PillowSystem pillow;

    pillow.tellRule("Triangle", "EqualToTriangleB2MB3");
    pillow.tell("EqualToSegmentA1A2(EqualToTriangleB2MB3)", IF);
    pillow.tell("EqualToAngleM(EqualToTriangleB2MB3)", IF);

    pillow.tell("AllAnglesAndSidesAreEqualToTrinagleB2MB3(EqualToTriangleB2MB3)", ISA);


    pillow.tell("CrossAngleSides(AOB)", ISA);
    pillow.addAttr("CrossAngleSides", "Cross two sides of angle AOB"
                                      "by 3 parallel straight: A1B1, A2B2 and A3B3");

    pillow.tell("ContainThreeParallelStraights(CrossAngleSides)", ISA);
    pillow.tell("ContainTwoEqualSegments(CrossAngleSides)", ISA);

    pillow.tell("A1B1(ContainThreeParallelStraights)", CONTAINS);
    pillow.tell("A2B2(ContainThreeParallelStraights)", CONTAINS);
    pillow.tell("A3B3(ContainThreeParallelStraights)", CONTAINS);

    pillow.tell("Straight(A1B1)", ISA);
    pillow.tell("Straight(A2B2)", ISA);
    pillow.tell("Straight(A3B3)", ISA);


    pillow.tell("A1A2(ContainTwoEqualSegments)", CONTAINS);
    pillow.tell("A2A3(ContainTwoEqualSegments)", CONTAINS);

    pillow.tell("EqualToSegmentA1A2(A2A3)", ISA);
    pillow.tell("Segment(A1A2)", ISA);
    pillow.tell("Segment(A2A3)", ISA);



    pillow.tell("DrawStraightsThatParallelToSideAngleSide(AOB)", ISA);

    pillow.tell("ContainTwoParallelStraights(DrawStraightsThatParallelToSideAngleSide)", ISA);
    pillow.tell("FormAngles(DrawStraightsThatParallelToSideAngleSide)", ISA);
    pillow.tell("FormParallelograms(DrawStraightsThatParallelToSideAngleSide)", ISA);
    pillow.tell("FormTriangles(DrawStraightsThatParallelToSideAngleSide)", ISA);

    pillow.tell("B1L(ContainTwoParallelStraights)", CONTAINS);
    pillow.tell("B2M(ContainTwoParallelStraights)", CONTAINS);

    pillow.tell("ContainAngles(FormAngles)", ISA);
    pillow.tell("AppropriateWithParallels(FormAngles)", ISA);

    pillow.tell("K(ContainAngles)", CONTAINS);
    pillow.tell("KLM(ContainAngles)", CONTAINS);
    pillow.tell("M(ContainAngles)", CONTAINS);
    pillow.tell("KB1B2(ContainAngles)", CONTAINS);
    pillow.tell("MB2B3(ContainAngles)", CONTAINS);

    pillow.tell("Angle(K)", ISA);
    pillow.tell("Angle(KLM)", ISA);
    pillow.tell("Angle(M)", ISA);
    pillow.tell("Angle(KB1B2)", ISA);
    pillow.tell("Angle(MB2B3)", ISA);

    pillow.tell("EqualToAngleK(KLM)", ISA);
    pillow.tell("EqualToAngleK(M)", ISA);
    pillow.tell("EqualToAngleK(KB1B2)", ISA);
    pillow.tell("EqualToAngleK(MB2B3)", ISA);


    pillow.tell("ContainParallelograms(FormParallelograms)", ISA);
    pillow.tell("A1A2KB1(ContainParallelograms)", ISA);
    pillow.tell("A2A3MB2(ContainParallelograms)", ISA);

    pillow.tell("Parallelogram(A1A2KB1)", ISA);
    pillow.tell("Parallelogram(A2A3MB2)", ISA);
    pillow.tell("A1A2(A1A2KB1)", CONTAINS);
    pillow.tell("A2K(A1A2KB1)", CONTAINS);
    pillow.tell("KB1(A1A2KB1)", CONTAINS);
    pillow.tell("A1B1(A1A2KB1)", CONTAINS);

    pillow.tell("A2A3(A2A3MB2)", CONTAINS);
    pillow.tell("A3M(A2A3MB2)", CONTAINS);
    pillow.tell("B2M(A2A3MB2)", CONTAINS);
    pillow.tell("A2B2(A2A3MB2)", CONTAINS);

    pillow.tell("EqualToSegmentA1A2(KB1)", ISA);
    pillow.tell("EqualToSegmentA1A2(B2M)", ISA);


    pillow.tell("ContainTriangles(FormTriangles)", ISA);
    pillow.tell("B1KB2(ContainTriangles)", CONTAINS);
    pillow.tell("B2MB3(ContainTriangles)", CONTAINS);

    pillow.tell("Triangle(B1KB2)", ISA);
    pillow.tell("Triangle(B2MB3)", ISA);
    pillow.tell("KB1(B1KB2)", CONTAINS);
    pillow.tell("KB2(B1KB2)", CONTAINS);
    pillow.tell("B1B2(B1KB2)", CONTAINS);
    pillow.tell("K(B1KB2)", CONTAINS);
    pillow.tell("KB1B2(B1KB2)", CONTAINS);

    pillow.tell("B2B3(B2MB3)", CONTAINS);
    pillow.tell("B2M(B2MB3)", CONTAINS);
    pillow.tell("MB3(B2MB3)", CONTAINS);

    pillow.tell("EqualToAngleM(K)", ISA);

    pillow.results("B1KB2");
}

int main()
{  
    ThalesTheorem();


    return 0;
}
