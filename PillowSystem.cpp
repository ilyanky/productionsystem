#include "PillowSystem.h"
#include <QFile>
#include <QStringList>
#include <QDebug>
#include <string>
#include <QRegExp>
#include <QStack>
#include <QVector>
#include <QString>
#include <HelpAlgorithms.h>


PillowSystem::PillowSystem()
{
}


void PillowSystem::parseExpression(const QString& exp)
{
    int index = getPriorityOper(exp);
    qDebug() << index;
    if ( exp.at(index) == 'o' )
        m_parseTree.addOperation("or");
    else if ( exp.at(index) == 'a' )
        m_parseTree.addOperation("and");
    else /* if ( exp.at(index) == 'n' ) */
        m_parseTree.addOperation("not");

    QString lvalue;
    QString rvalue;
    if ( exp.at(index) == 'n' ) {
        rvalue = getSideValue(exp, index, Right);

//        qDebug() << lvalue << rvalue;
        if ( isAtomic(rvalue) )
            m_parseTree.addValue(rvalue);
        else
            parseExpression(rvalue);

        m_parseTree.returnToParent();
    }
    else {
        lvalue = getSideValue(exp, index, Left);
        rvalue = getSideValue(exp, index, Right);

//        qDebug() << lvalue << rvalue;
        if ( isAtomic(rvalue) )
            m_parseTree.addValue(rvalue);
        else
            parseExpression(rvalue);

        if ( isAtomic(lvalue) )
            m_parseTree.addValue(lvalue);
        else
            parseExpression(lvalue);

        m_parseTree.returnToParent();
    }
}


bool PillowSystem::isErrorExpression(const QString& exp)
{
    if ( exp.count('(') != exp.count(')') )
        return false;

    return true;
}


QString PillowSystem::getSideValue(const QString& exp, const int& index, const Side& side)
{
    QString res = exp;

//    qDebug() << res;

    if ( side == Left )
        res = res.mid(0, index);
    else /*if ( side == Right )*/ {
        if ( exp.at(index) == 'o' )
            res = res.mid(index + 2, res.size());
        else if ( exp.at(index) == 'a' || exp.at(index) == 'n' )
            res = res.mid(index + 3, res.size());
    }

    res.replace(" ", "");
    if ( res.count('(') != res.count(')') ) {
        if ( side == Left )
            res.remove(0, 1);
        else
            res.remove(res.size() - 1, 1);
    }

    return res;
}


bool PillowSystem::isAtomic(const QString& exp)
{
    if ( containsOper(exp, 0, exp.size() - 1) )
        return false;

    return true;
}


void PillowSystem::tempShow()
{
    m_parseTree.showAll();
}


bool PillowSystem::tempIsIdenity()
{
    return m_parseTree.helper();
}


void PillowSystem::tell(const QString& exp, const LinkType& type)
{
    QString predicateName = exp.left(exp.indexOf('('));
    QString expr = exp.right(exp.size() - exp.indexOf('('));
//    qDebug() << predicateName;
//    qDebug() << expr;

    QString name = expr;
    name.remove(0, 1);
    name.remove(name.size() - 1, 1);

    if ( type == ISA )
        m_frameGraph.isa_link(predicateName.toStdString(), name.toStdString());
    else if ( type == IF )
        m_rulesGraph.if_link(predicateName.toStdString(), name.toStdString());
    else
        m_frameGraph.contains_link(predicateName.toStdString(), name.toStdString());

    analyseRules("Triangle");

//    qDebug() << "------------------------";
//    showGraph2();
//    showGraphNames();
}


void PillowSystem::showGraph()
{
    m_frameGraph.show();
}


void PillowSystem::showGraph2()
{
    m_frameGraph.show2();
}


void PillowSystem::showGraphNames()
{
    m_frameGraph.showNames();
}


void PillowSystem::showRuleGraph2()
{
    m_rulesGraph.show2();
}


void PillowSystem::showRuleGraphNames()
{
    m_rulesGraph.showNames();
}


std::string PillowSystem::results(const std::string& vertexName)
{
    std::pair<bool, Vertex> pr = m_frameGraph.contains(vertexName);
    if ( !pr.first )
        return std::string("there is no such vertex");

    std::set<VertexInfo> set;
    m_frameGraph.convolve(pr.second, &set, ISA);

    //temp
    for ( std::set<VertexInfo>::iterator i = set.begin();
          i != set.end(); ++i )
        qDebug() << i->name.c_str() << i->linkType;
                    //<< vector.at(i).attrs.at(0).c_str();

    return "false";
}


void PillowSystem::addAttr(const std::string& vertexName, const std::string& attr)
{
    m_frameGraph.addAttr(vertexName, attr);
}


void PillowSystem::tellRule(const std::string& vertexName, const std::string& ruleName)
{
    Vertex v = m_rulesGraph.addVertex(ruleName);
    m_rules[vertexName] = v;
}


void PillowSystem::analyseRules(const std::string& temp)
{
    using namespace boost;

    if ( m_rules.empty() )
        return;

    std::list<Vertex> list = m_frameGraph.getVertices(temp);
    for ( auto i : list ) {
        std::set<VertexInfo> info;
        m_frameGraph.convolve(i, &info, ISA);

        VertexInfo t;
        t.name = m_rulesGraph.getName(m_rules[temp]);
        if ( info.count(t) == 0 )
        {
            std::pair<OutEdgeIter, OutEdgeIter> pe =
                    m_rulesGraph.getOutEdges(m_rules[temp]);
            for ( ; pe.first != pe.second; ++pe.first ) {
                VertexInfo t;
                t.name = m_rulesGraph.targetName(*pe.first);
                if ( info.count(t) == 0 )
                    break;
            }

            if ( pe.first == pe.second ) {
                QString exp;
                exp.push_back(m_rulesGraph.getName(m_rules[temp]).c_str());
                exp.push_back("(");
                exp.push_back(m_frameGraph.getName(i).c_str());
                exp.push_back(")");
                tell(exp, ISA);
            }
        }
    }
}
